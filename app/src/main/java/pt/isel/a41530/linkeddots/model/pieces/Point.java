package pt.isel.a41530.linkeddots.model.pieces;

public class Point {

    private final int X, Y;

    public Point(int x, int y){
        this.X = x;
        this.Y = y;
    }

    public int getX(){
        return X;
    }

    public int getY(){
        return Y;
    }
}
