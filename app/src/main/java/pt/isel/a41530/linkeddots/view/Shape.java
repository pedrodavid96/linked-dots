package pt.isel.a41530.linkeddots.view;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;

public enum Shape {

    CIRCLE {
        @Override
        public void draw(Canvas canvas, int side, Paint paint) {
            canvas.drawCircle(side/2, side/2, side/2 - side/8, paint);
        }
    },
    SQUARE {
        @Override
        public void draw(Canvas canvas, int side, Paint paint) {
            canvas.drawRect(0, 0, side, side, paint);
        }
    },
    ROUND_SQUARE {
        @Override
        public void draw(Canvas canvas, int side, Paint paint) {
            canvas.drawRect(0, 0, side, side, paint);
            paint.setColor(Color.BLACK);
            canvas.drawLine(0, 0, side, side, paint);
            canvas.drawLine(0, side, side, 0, paint);
        }
    },
    X_SQUARE {
        @Override
        public void draw(Canvas canvas, int side, Paint paint) {
            RectF r = new RectF(0, 0, side, side);
            canvas.drawRoundRect(r, 20, 20, paint);
        }
    };

    public abstract void draw(Canvas canvas, int side, Paint paint);

    public static void draw(Shape shape, Canvas canvas, int side, Paint paint){
        for(Shape type : Shape.values()){
            if(type == shape)
                shape.draw(canvas, side, paint);
        }
    }
}
