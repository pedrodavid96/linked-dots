package pt.isel.a41530.linkeddots.view.utils;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;

import pt.isel.a41530.linkeddots.R;

public class FinishScreen extends TextView {

    private static final int DEFAULT_TEXT_SIZE = 40;

    public FinishScreen(Context context, boolean win) {
        super(context, win?
                context.getResources().getString(R.string.win):
                context.getResources().getString(R.string.lose));
        setTextSize(DEFAULT_TEXT_SIZE);
        setTextColor(win? Color.argb(0xF0, 0x23, 0x91, 0x30) : Color.argb(0xF0, 0x91, 0x23, 0x30));
        setGravity(Gravity.CENTER);
    }
}
