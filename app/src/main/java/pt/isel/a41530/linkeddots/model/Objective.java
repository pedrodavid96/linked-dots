package pt.isel.a41530.linkeddots.model;

public class Objective {

    private final char type;
    private final int objective;
    private int progress = 0;

    Objective(char type, int amount) {
        this.type = type;
        this.objective = amount;
    }

    public char getType() {
        return type;
    }

    public int getObjective() {
        return objective;
    }

    public int getProgress(){
        return progress;
    }

    public void increase(int progress){
        this.progress += progress;
    }

    public boolean isCompleted(){
        return progress>=objective;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    @Override
    public String toString(){
        String obj = String.valueOf((progress == 0)? objective : (objective + "/" + progress));
        return type + obj;
    }
}
