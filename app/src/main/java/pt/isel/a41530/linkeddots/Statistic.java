package pt.isel.a41530.linkeddots;

import java.util.Calendar;

public class Statistic implements Comparable<Statistic>{

    private final int LEVEL;
    private int score = 0;
    private String user = "", date = "";

    public Statistic(String[] values){
        this(values[0]);
        score = Integer.valueOf(values[1]);
        if(score!=0){
            this.user = values[2];
            this.date = values[3];
        }
    }

    public Statistic(String level){
        this(Integer.valueOf(level));
    }

    public Statistic(int level){
        this.LEVEL = level;
    }

    public int getLevel() {
        return LEVEL;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getDate() {
        return date;
    }

    public void setCurrentDate(){
        Calendar c = Calendar.getInstance();
        this.date = String.format("%02d/%02d/%04d", c.get(Calendar.DAY_OF_MONTH),
                        c.get(Calendar.MONTH) + 1, c.get(Calendar.YEAR));
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String toString(){
        return LEVEL + " " + score + " " + user + " " + date;
    }

    @Override
    public int compareTo(Statistic compareStatistic) {
        return this.LEVEL - compareStatistic.getLevel();
    }
}
