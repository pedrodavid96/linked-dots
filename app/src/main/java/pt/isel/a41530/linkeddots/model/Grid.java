package pt.isel.a41530.linkeddots.model;

import pt.isel.a41530.linkeddots.model.pieces.PieceType;

import static pt.isel.a41530.linkeddots.model.OnGameActionListener.PieceAction.*;

public class Grid{

    private final int COLS, LINES, PIECE_TYPES;
    private char[][] grid;
    private static final int NEW_FROM = 0;
    private OnGameActionListener listener;

    public Grid(int cols, int lines, int pieceTypes){
        this.COLS = cols;
        this.LINES = lines;
        this.PIECE_TYPES = pieceTypes;
        grid = new char[COLS][LINES];
    }

    int clearPiecesOfType(char type){
        int counter = 0;
        for (int c = 0; c < COLS; c++)
            for(int l = LINES - 1; l >= 0; l--)
                if(grid[c][l] != ' ' && grid[c][l] == type) {
                    clearPiece(c, l);
                    counter++;
                }
        return counter;
    }

    void newPieces(){
        boolean hasNext;
        for (int c = 0; c < COLS; c++) {                        //for each column
            hasNext = true;
            for (int l = LINES-1; l >= 0; l--) {                //for each line
                if(grid[c][l] == ' ' && hasNext)                //check if is null(and still "has" next)
                    hasNext = checkColAndShift(c, l);           //check if truly has next and shift
                if(!hasNext && grid[c][l] == ' ') {             //if there is no more pieces
                    grid[c][l] = newRandomPiece(PIECE_TYPES);   //bring a new random piece
                    listener.pieceAction(c, NEW_FROM, l, NEW); //notify that there is a new piece
                }
            }
        }
    }

    private boolean checkColAndShift(int column, int lineOffset){
        for(int line = lineOffset - 1; line >= 0; line--){      //for each line (starting in the offset)
            if(grid[column][line] != ' '){                      //if the line != null "hasNext"
                if(PieceType.valueOf(grid[column][line]).isMovable()) {  // Shift Piece in model and view
                    grid[column][lineOffset] = grid[column][line];
                    listener.pieceAction(column, line, lineOffset, SHIFT);
                    grid[column][line] = ' ';
                    return true;
                }
            }
        }
        return false;
    }

    void clearPiece(int c, int l) {
        grid[c][l] = ' ';
        listener.pieceAction(c, l, 0, CLEAR);
    }

    void addPiece(char type, int c, int l){
        grid[c][l] = type;
    }

    static char newRandomPiece(int pieceTypes){
        return PieceType.createPiece((int)(Math.random() * pieceTypes));
    }

    public char getPiece(int c, int l) {
        return grid[c][l];
    }

    public int getCols(){
        return COLS;
    }
    public int getLines(){
        return LINES;
    }

    public void setOnPlayListener(OnGameActionListener listener){
        this.listener = listener;
    }

    @Override
    public String toString(){
        String model = "";
        for (int l = 0; l < LINES; l++) {
            for (int c = 0; c < COLS; c++) {
                model += grid[c][l] + " ";
            }
            model += "\n";
        }
        return model;
    }
}
