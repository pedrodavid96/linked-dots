package pt.isel.a41530.linkeddots;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

import pt.isel.a41530.lib.tile.Animator;
import pt.isel.a41530.lib.tile.OnTileTouchListener;
import pt.isel.a41530.lib.tile.TilePanel;
import pt.isel.a41530.linkeddots.model.IOManager;
import pt.isel.a41530.linkeddots.model.LevelManager;
import pt.isel.a41530.linkeddots.model.LinkedDots;
import pt.isel.a41530.linkeddots.model.Objective;
import pt.isel.a41530.linkeddots.model.OnGameActionListener;
import pt.isel.a41530.linkeddots.view.ObjectiveView;
import pt.isel.a41530.linkeddots.view.PieceView;
import pt.isel.a41530.linkeddots.view.utils.FinishScreen;
import pt.isel.a41530.linkeddots.view.utils.ScoreSubmitScreen;

public class DotsActivity extends Activity implements OnTileTouchListener, OnGameActionListener {

    private static final int PIECE_SPEED = 400, LINE_FROM = -1,  LINE_DEST = 2 /*+ size*/;
    private static final String TEMP_SAVE = "default.txt";
    private LinkedDots model;
    private LevelManager manager;
    private TilePanel panel;
    private Animator anim;
    private TextView playsView, scoreView;
    private ArrayList<ObjectiveView> objectiveViews = new ArrayList<>();

    private int level, maxScore;
    private HighScoreTable highScoreTable;
    private ArrayList<Integer> levels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dots);
        panel = (TilePanel) findViewById(R.id.panel);
        Button exit = (Button) findViewById(R.id.exit);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exit();
            }
        });

        Intent intent = getIntent();
        level = intent.getIntExtra(MainActivity.LEVEL_KEY, -1);
        levels = intent.getIntegerArrayListExtra(MainActivity.LEVELS_KEY);
        loadHighScoreTable();
        newGame();
    }

    private boolean checkHighScore(){
        return (model.getScore()> maxScore);
    }

    private void exit(){
        saveFile();
        saveHighScoreTable();
        finish();
    }

    /* Create a Game */
    private void newGame(){
        if(level > MainActivity.RANDOM_GAME){
            loadLevelFromAssets(String.valueOf(level) + ".txt");
        }
        else if(level == MainActivity.RANDOM_GAME) {
            manager = new LevelManager();
        }else if(level == MainActivity.CONT_GAME){
            loadLevelFromSave();
        }
        maxScore = highScoreTable.getMaxScore(level);
        panel.setSize(manager.getCols(), manager.getLines());
        loadGame();
    }

    /* Loads the Game that is in memory / created to the activity */
    private void loadGame(){
        model = new LinkedDots(manager);
        model.setOnPlayListener(this);

        /* Check objectives so it doesn't show the dialog in case of resume or no objective*/
        model.hasWon();

        panel.setListener(this);
        anim = panel.getAnimator();

        loadHUD(manager);
        updateGridView();
        updateScoreView();
    }

    /* General Activity View Methods */

    private void loadHUD(LevelManager manager) {
        playsView = (TextView) findViewById(R.id.plays);

        LinearLayout objectiveViews = (LinearLayout) findViewById(R.id.objectives);
        objectiveViews.removeAllViews();

        Objective[] objectives = manager.getObjectives();
        if (objectives != null){
            for (Objective objective : objectives) {
                ObjectiveView tempObj = new ObjectiveView(this, objective);
                objectiveViews.addView(tempObj.getRoot());
                this.objectiveViews.add(tempObj);
            }
        }

        scoreView = (TextView) findViewById(R.id.score);
    }

    public void updateGridView(){
        for (int c = 0; c < model.getCols(); c++)
            for(int l = 0; l < model.getLines(); l++)
                panel.setTile(c, l, PieceView.valueOf(model.getPiece(c, l)));
    }

    public void updateScoreView(){
        scoreView.setText(String.valueOf(model.getScore()));
        playsView.setText(String.valueOf(model.getPlays()));
        for (ObjectiveView view : objectiveViews)
            view.updateObjective();
    }

    /* I/O Methods */

    protected void saveFile() {
        try(BufferedWriter writer = new BufferedWriter
                (new OutputStreamWriter(openFileOutput(TEMP_SAVE, MODE_PRIVATE)))){
            writer.write(level + " ");
            writer.newLine();
            IOManager.saveLevel(writer, model);
        }catch (IOException e){
            e.printStackTrace();
        }
    }


    protected void loadLevelFromSave() {
        try(BufferedReader reader = new BufferedReader
                (new InputStreamReader(openFileInput(TEMP_SAVE)))){
            String[] values = reader.readLine().split(" ");
            level = Integer.valueOf(values[0]);
            manager = IOManager.loadLevel(reader);
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    protected void loadLevelFromAssets(String name){
        try(BufferedReader reader = new BufferedReader
                (new InputStreamReader(getAssets().open("levels/" + name)))){
            manager = IOManager.loadLevel(reader);
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    protected void loadHighScoreTable(){
        try(BufferedReader reader = new BufferedReader
                (new InputStreamReader(openFileInput(MainActivity.HIGH_SCORE_TABLE)))) {
            highScoreTable = new HighScoreTable(reader, levels);
        }catch (FileNotFoundException e){
            highScoreTable = new HighScoreTable();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    protected void saveHighScoreTable(){
        try(BufferedWriter writer = new BufferedWriter
                (new OutputStreamWriter(openFileOutput(MainActivity.HIGH_SCORE_TABLE, MODE_PRIVATE)))){
            highScoreTable.save(writer);
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        saveFile();
        saveHighScoreTable();
        getIntent().putExtra(MainActivity.LEVEL_KEY, MainActivity.CONT_GAME);
    }

    /* TOUCH LISTENERS */

    @Override
    public boolean onClick(int x, int y) {
        return true;
    }

    @Override
    public boolean onDrag(int x, int y, int xTo, int yTo) {
        PieceView p = (PieceView) panel.getTile(xTo, yTo);
        if(model.addLink(xTo, yTo)){
            p.toggleSelect();
            panel.invalidate(xTo,yTo);
        }
        return true;
    }

    @Override
    public void onDragEnd(int x, int y) {
        model.finishCombo();
    }

    @Override
    public void onDragCancel() {
        model.finishCombo();
    }

    @Override
    public void onDown(int x, int y) {
        PieceView p = (PieceView) panel.getTile(x, y);
        if(model.newCombo(x, y)) {
            p.toggleSelect();
            panel.invalidate(x, y);
        }
    }

    /* GAME LISTENER */

    @Override
    public void pieceAction(int col, int lineOrigin, int lineDest, PieceAction action){
        switch(action){
            case NEW:
                anim.entryTile(col, LINE_FROM, col, lineDest, PIECE_SPEED,
                        PieceView.valueOf(model.getPiece(col, lineDest)));
                break;
            case SHIFT:
                anim.floatTile(col, lineOrigin, col, lineDest, PIECE_SPEED);
                break;
            case CLEAR:
                anim.exitTile(col, lineOrigin, col,
                        panel.getHeightInTiles()+LINE_DEST, PIECE_SPEED);
                break;
            case UNDO:
                PieceView p = (PieceView) panel.getTile(col, lineOrigin);
                p.toggleSelect();
                panel.invalidate(col, lineOrigin);
                break;
        }
    }

    @Override
    public void play() {
        updateScoreView();
    }

    @Override
    public void onGameFinish(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LinearLayout layout = new LinearLayout(this);
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.addView(new FinishScreen(this, model.hasWon()));
        final ScoreSubmitScreen highScore = new ScoreSubmitScreen(this);
        if(model.hasWon()) {
            if(checkHighScore()){
                layout.addView(highScore);
            }
            if(levels!=null) {
                builder.setPositiveButton(getResources().getString(R.string.nextLevel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        saveScore(highScore.getNickName());
                        int nextLevelIndex = (levels.indexOf(level) + 1) % levels.size();
                        level = levels.get(nextLevelIndex);
                        newGame();
                    }
                });
            }
        }
        if(model.getPlays()!=0) {
            builder.setNeutralButton(getResources().getString(R.string.continueGame), null);
        }
        builder.setNegativeButton(getResources().getString(R.string.playAgain), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                saveScore(highScore.getNickName());
                newGame();
            }
        });
        builder.setView(layout).setCancelable(false).create().show();
    }

    private void saveScore(String user){
        if(checkHighScore()){
            highScoreTable.addStatistic(level, model.getScore(), user);
        }
    }
}