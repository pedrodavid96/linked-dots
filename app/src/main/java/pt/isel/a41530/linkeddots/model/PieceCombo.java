package pt.isel.a41530.linkeddots.model;

import java.util.LinkedList;

import pt.isel.a41530.linkeddots.model.pieces.Point;

public class PieceCombo extends LinkedList<Point> {

    private static final int MINIMUM_COMBO_SIZE = 2;

    public void addPiece(int x, int y){
        add(new Point(x, y));
    }

    public boolean validMovement(int x, int y){
        if(size() > 0)
            return (Math.abs(x - getLastX()) == 1 && y - getLastY() == 0)
                    || (Math.abs(y - getLastY()) == 1 && x - getLastX() == 0);
        return false;
    }

    public boolean checkUndo(int x, int y){
        return size() > 1 &&
                x == previous().getX() &&
                y == previous().getY();
    }

    public boolean checkCollision(int x, int y){
        Point testPoint = new Point(x, y);
        for(Point point: this){
            if(testPoint.getX() == point.getX() && testPoint.getY() == point.getY())
                return true;
        }
        return false;
    }

    public int getFirstX(){
        return getFirst().getX();
    }

    public int getFirstY() {
        return getFirst().getY();
    }

    public int getLastX(){
        return getLast().getX();
    }

    public int getLastY() {
        return getLast().getY();
    }

    public Point previous(){
        return get(size()-2);
    }

    public boolean canFinish(){
        return size() >= MINIMUM_COMBO_SIZE;
    }

    public boolean isSpecial(int x, int y){
        return x == getFirstX() && y == getFirstY();
    }
}
