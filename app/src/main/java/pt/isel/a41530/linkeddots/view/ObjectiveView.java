package pt.isel.a41530.linkeddots.view;

import android.app.Activity;
import android.widget.LinearLayout;
import android.widget.TextView;

import pt.isel.a41530.linkeddots.R;
import pt.isel.a41530.linkeddots.model.Objective;

public class ObjectiveView {

    private ObjectiveType piece;
    private TextView text;
    private Objective objective;
    private LinearLayout root;

    boolean finished = false;

    public ObjectiveView(Activity context, Objective objective) {
        this.objective = objective;
        root = (LinearLayout) context.getLayoutInflater().inflate(R.layout.objective, null);
        text = (TextView) root.findViewById(R.id.objectiveText);
        piece = (ObjectiveType) root.findViewById(R.id.objectiveType);
        piece.setPiece(objective.getType());
    }

    public LinearLayout getRoot() {
        return root;
    }

    public void updateObjective(){
        if(!finished && objective.getProgress() >= objective.getObjective()){
            finished = true;
            text.setTextSize(17);
        }
        String s = String.valueOf(finished? objective.getProgress() :
                (objective.getProgress() + "/" + objective.getObjective()));
        text.setText(s);
    }
}
