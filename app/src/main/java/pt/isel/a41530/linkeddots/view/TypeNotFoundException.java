package pt.isel.a41530.linkeddots.view;

public class TypeNotFoundException extends NullPointerException {

    private final char TYPE;

    public TypeNotFoundException(char c){
        super();
        this.TYPE = c;
    }

    public String toString(){
        return "Type properties not set for type " + TYPE + ".\n" +
                "Create properties on pt.isel.a41530.linkeddots.view.PieceViewProperties.java";
    }
}
