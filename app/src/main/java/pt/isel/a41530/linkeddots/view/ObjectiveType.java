package pt.isel.a41530.linkeddots.view;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;

public class ObjectiveType extends View {

    private PieceView circle;

    public ObjectiveType(Context context) {
        super(context);
    }

    public ObjectiveType(Context context, AttributeSet attributes) {
        super(context, attributes);
    }

    @Override
    public void onDraw(Canvas canvas) {
        if(circle!=null)
            circle.draw(canvas, getHeight());
    }

    public void setPiece(char piece) {
        circle = PieceView.valueOf(piece);
    }
}
