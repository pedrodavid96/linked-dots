package pt.isel.a41530.linkeddots.model.pieces;

public enum PieceType {

    BASIC('A', 7),
    ALL_TYPE('?'){
        @Override
        public boolean alwaysConnects() {
            return true;
        }
    }, BLOCK('#')
    , N_BLOCK('X'){
        @Override
        public boolean isMovable() {
            return false;
        }
    };

    /* LOGIC PROPERTIES */
    public boolean alwaysConnects(){
        return false;
    }
    public boolean isMovable(){
        return true;
    }

    private char NAME_ID;
    private final int NUMBER_OF_PIECES;

    private PieceType(char nameID, int numberOfPieces){
        this.NUMBER_OF_PIECES = numberOfPieces;
        this.NAME_ID = nameID;
    }
    private PieceType(char nameID){
        this(nameID, 1);
    }

    public static PieceType valueOf(char c){
        for(PieceType type: values()){
            for(int i = 0; i < type.NUMBER_OF_PIECES; i++)
                if(type.NAME_ID + i == c) {
                    return type;
                }
        }
        return null;
    }

    public static char createPiece(int rng){
        return (char) ('A' + rng);
    }
}