package pt.isel.a41530.linkeddots;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;

import pt.isel.a41530.linkeddots.view.utils.OrientedLayout;
import pt.isel.a41530.linkeddots.view.utils.TextButton;
import pt.isel.a41530.linkeddots.view.utils.TextView;

public class MainActivity extends Activity implements View.OnClickListener{

    public static final String HIGH_SCORE_TABLE = "highScore.txt";
    public static final String LEVEL_KEY = "LEVEL", LEVELS_KEY = "LEVELS";
    public static final int CONT_GAME = -1, RANDOM_GAME = 0;
    private Button newGame, contGame, levels;
    private ArrayList<Integer> levelList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainMenu();
    }

    private void mainMenu(){
        setContentView(R.layout.activity_main);

        newGame = (Button) findViewById(R.id.newGame);
        contGame = (Button) findViewById(R.id.contGame);
        levels = (Button) findViewById(R.id.thoth);

        newGame.setOnClickListener(this);
        contGame.setOnClickListener(this);
        levels.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setContentView(loadLevels());
            }
        });
    }

    private LinearLayout loadLevels() {
        String[] fileNames = new String[0];
        try {
            fileNames = getAssets().list("levels");
        } catch (IOException e) {
            e.printStackTrace();
        }

        LinearLayout levels = new OrientedLayout(this, LinearLayout.VERTICAL);
        levels.addView(new TextView(this, levelInfoBar()));
        ArrayList<Statistic> statistics = new ArrayList<>();
        for(String name: fileNames){
            name = name.replace(".txt", "");
            statistics.add(loadLevel(name));
        }
        Collections.sort(statistics);
        return processStatistics(statistics);
    }

    protected String levelInfoBar(){
        return String.format("%-10s%-6s%-14s%s",
                this.getResources().getString(R.string.level),
                this.getResources().getString(R.string.score),
                this.getResources().getString(R.string.user),
                this.getResources().getString(R.string.date));
    }

    private Statistic loadLevel(String levelName) {
        try (BufferedReader reader = new BufferedReader
                (new InputStreamReader(openFileInput(HIGH_SCORE_TABLE)))) {
            String tempString; //format: Level Number, HighScore, Name, Date
            String[] values;
            while ((tempString = reader.readLine()) != null) {
                values = tempString.split(" ");
                if (levelName.equals(values[0]))
                    return new Statistic(values);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new Statistic(levelName);
    }

    private LinearLayout processStatistics(ArrayList<Statistic> statistics){
        LinearLayout levels = new OrientedLayout(this, LinearLayout.VERTICAL);
        levels.addView(new TextView(this, levelInfoBar()));
        for(Statistic statistic : statistics){
            levelList.add(statistic.getLevel());
            levels.addView(new LevelInfoLayout(this, statistic));
        }
        TextButton home = new TextButton(this, getResources().getString(R.string.back));
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainMenu();
            }
        });
        levels.addView(home);
        return levels;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mainMenu();
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(this, DotsActivity.class);
        intent.putExtra(LEVEL_KEY, view == contGame? CONT_GAME : RANDOM_GAME);
        startActivity(intent);
    }

    protected class LevelButton extends TextButton {

        public LevelButton(Context context, final int level){
            super(context, String.format("%-6s %-2d",
                    context.getResources().getString(R.string.level), level));
            setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(MainActivity.this, DotsActivity.class);
                    intent.putExtra(LEVEL_KEY, level);
                    intent.putExtra(LEVELS_KEY, levelList);
                    startActivity(intent);
                    levelList.clear();
                }
            });
        }
    }

    protected class LevelInfoLayout extends LinearLayout{

        public LevelInfoLayout(Context context, Statistic statistic){
            super(context);
            setOrientation(HORIZONTAL);
            addView(new LevelButton(context, statistic.getLevel()));
            String details = String.format("%-6d%-14s%s",
                    statistic.getScore(), statistic.getUser(), statistic.getDate());
            addView(new TextView(context, details));
        }
    }
}
