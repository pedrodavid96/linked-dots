package pt.isel.a41530.linkeddots.view.utils;

import android.content.Context;
import android.widget.LinearLayout;

public class OrientedLayout extends LinearLayout {
    public OrientedLayout(Context context, int orientation) {
        super(context);
        setOrientation(orientation);
    }
}
