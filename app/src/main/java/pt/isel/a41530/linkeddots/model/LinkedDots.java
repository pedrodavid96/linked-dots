package pt.isel.a41530.linkeddots.model;

import pt.isel.a41530.linkeddots.model.pieces.PieceType;
import pt.isel.a41530.linkeddots.model.pieces.Point;

import static pt.isel.a41530.linkeddots.model.OnGameActionListener.PieceAction.*;

public class LinkedDots {

    private static final boolean SIMPLE = false, CONNECTED = true;

    private Grid grid;
    private PieceCombo combo;
    private LevelManager manager;
    private OnGameActionListener listener;

    public LinkedDots(LevelManager manager){
        this.manager = manager;
        grid = new Grid(manager.getCols(), manager.getLines(), manager.getPieceTypes());
        combo = new PieceCombo();
        manager.newLevel(this);
    }

    public boolean newCombo(int c, int l){
        if(PieceType.valueOf(grid.getPiece(c, l)) == PieceType.BASIC){
            combo.addPiece(c, l);
            return true;
        }
        return false;
    }

    public boolean addLink(int c, int l){
        if(combo.validMovement(c, l)){
            if(combo.checkUndo(c, l)){
                listener.pieceAction(combo.getLastX(), combo.getLastY(), 0, UNDO);
                combo.removeLast();
            }
            else if(combo.checkCollision(c, l) && combo.isSpecial(c, l)){
                finishCombo(CONNECTED);
            }
            else if (checkTypes(c, l)){
                combo.addPiece(c, l);
                return true;
            }
        }
        return false;
    }

    private boolean checkTypes(int c, int l){
        return grid.getPiece(c, l) == firstPieceType()
                || PieceType.valueOf(grid.getPiece(c, l)).alwaysConnects();
    }

    private Character firstPieceType() {
        return grid.getPiece(combo.getFirstX(), combo.getFirstY());
    }

    public boolean finishCombo(){
        if(combo.canFinish()) {
            finishCombo(SIMPLE);
            return true;
        }
        if(!combo.isEmpty())
            for (Point piece : combo) //Remove selected from all views
                listener.pieceAction(piece.getX(), piece.getY(), 0, UNDO);
                //In this case, min = 2 so this only runs once
        combo.clear();
        return false;
    }

    private void finishCombo(boolean type){
        char pieceType = firstPieceType();
        int counter = clearCombo();
        if(type == CONNECTED) //if type (is true)
            counter += grid.clearPiecesOfType(pieceType);
        combo.clear();
        grid.newPieces();
        manager.play(counter, pieceType);
        listener.play();
        checkGameState();
    }

    private void checkGameState(){
        if(!manager.hasPlaysLeft() || (!manager.hasWon() && manager.checkWinCondition()))
            listener.onGameFinish();
    }

    private int clearCombo(){
        for (Point piece: combo){
            grid.clearPiece(piece.getX(), piece.getY());
        }
        return combo.size();
    }

    /* Public Piece Getters / Setters */
    public void addPieceToCombo(int c, int l){
        combo.addPiece(c, l);
    }

    public void addPiece(char type, int c, int l){
        grid.addPiece(type, c, l);
    }
    public char getPiece(int c, int l) {
        return grid.getPiece(c, l);
    }

    /* General Getters */

    public int getCols(){
        return grid.getCols();
    }
    public int getLines(){
        return grid.getLines();
    }

    public int getScore(){
        return manager.getScore();
    }
    public int getPlays() {
        return manager.getPlaysLeft();
    }
    public boolean hasWon(){
        return manager.checkWinCondition();
    }
    public Objective[] getObjectives() {
        return manager.getObjectives();
    }
    public PieceCombo getCombo(){
        return combo;
    }

    /* SETTERS */
    public void setWin(boolean win){
        manager.setWin(win);
    }

    public void setOnPlayListener(OnGameActionListener listener){
        this.listener = listener;
        grid.setOnPlayListener(listener);
    }

    @Override
    public String toString(){
        return grid.toString();
    }
}