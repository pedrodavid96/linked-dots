package pt.isel.a41530.linkeddots.view;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

import pt.isel.a41530.lib.tile.Tile;

public class PieceView implements Tile {

    private static final int DEFAULT_TEXT_SIZE = 40, DEFAULT_WIDTH = 5,
            DEFAULT_COLOR = Color.BLACK;
    private static final Rect textBounds = new Rect();
    private static Paint paint = new Paint();
    static {
        paint.setStrokeWidth(DEFAULT_WIDTH);
        paint.setTextSize(DEFAULT_TEXT_SIZE);
    }

    private boolean selected;
    private char type;

    public PieceView(char type){
        this.type = type;
    }

    @Override
    public void draw(Canvas canvas, int side){
        drawPiece(canvas, side);
        drawText(canvas, side);
        drawSelected(canvas, side);
    }

    private void drawPiece(Canvas canvas, int side){
        paint.setColor(PieceViewProperties.getColor(type));
        PieceViewProperties.getShape(type).draw(canvas, side, paint);
        paint.setColor(DEFAULT_COLOR);
        paint.setTextSize(DEFAULT_TEXT_SIZE);
    }

    private void drawText(Canvas canvas, int side){
        String text = PieceViewProperties.getText(type);
        if(text.length()>0) {
            paint.getTextBounds(text, 0, text.length(), textBounds);
            canvas.drawText(text, side / 2 - textBounds.exactCenterX(), side / 2
                    - textBounds.exactCenterY(), paint);
        }
    }

    private void drawSelected(Canvas canvas, int side){
        if (selected) {
            paint.setStyle(Paint.Style.STROKE);
            PieceViewProperties.getShape(type).draw(canvas, side, paint);
            paint.setStyle(Paint.Style.FILL);
        }
    }

    public static PieceView valueOf(char type){
        return new PieceView(type);
    }

    @Override
    public boolean setSelect(boolean selected) {
        return false;
    }

    public void toggleSelect(){
        selected = !selected;
    }

    public String toString(){
        return String.valueOf(type);
    }
}