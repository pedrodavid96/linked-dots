package pt.isel.a41530.linkeddots.model;

public class LevelManager {

    public static final int DEFAULT_COLS = 6, DEFAULT_LINES = 6,
            DEFAULT_PLAYS_REMAINING = 15, DEFAULT_SCORE = 0, DEFAULT_PIECE_TYPES = 3;

    private final int cols, lines;
    private int playsLeft, score;
    private int pieceTypes;
    private Objective[] objectives;
    private String[] initialData;
    private boolean won;

    public LevelManager(){
        this(DEFAULT_COLS, DEFAULT_LINES, DEFAULT_PLAYS_REMAINING, DEFAULT_SCORE);
    }

    public LevelManager(int cols, int lines, int playsLeft, int score){
        this.cols = cols;
        this.lines = lines;
        this.playsLeft = playsLeft;
        this.score = score;
        pieceTypes = DEFAULT_PIECE_TYPES;
    }

    public void play(int score, char type){
        this.score += score;
        if (objectives!=null)
            for (Objective objective : objectives)
                if(objective.getType() == type)
                    objective.increase(score);
        playsLeft--;
    }

    public boolean checkWinCondition() {
        if (objectives != null) {
            for (Objective obj : objectives) {
                if (!obj.isCompleted())
                    return false;
            }
        }
        return won=true;
    }

    public void newLevel(LinkedDots model) {
        if(initialData != null)
            dataLevel(model);
        else
            randomLevel(model);
        won = false;
    }

    private void dataLevel(LinkedDots model) {
        for(int l = 0; l < lines; l++)
            for (int c = 0; c < cols; c++)
                model.addPiece(initialData[l].charAt(c), c, l);
    }

    private void randomLevel(LinkedDots model){
        for (int c = 0; c < cols; c++)
            for(int l = 0; l < lines; l++)
                model.addPiece(Grid.newRandomPiece(pieceTypes), c, l);
    }

    public int getCols(){
        return cols;
    }

    public int getLines(){
        return lines;
    }

    public int getScore(){
        return score;
    }

    public int getPieceTypes() {
        return pieceTypes;
    }

    public boolean hasPlaysLeft(){
        return playsLeft > 0;
    }

    public int getPlaysLeft(){
        return playsLeft;
    }

    public boolean hasWon(){
        return won;
    }

    public Objective[] getObjectives(){
        return objectives;
    }

    public void setWin(boolean win){
        this.won = win;
    }

    public void setObjectives(Objective[] obj){
        this.objectives = new Objective[obj.length];
        System.arraycopy(obj, 0, this.objectives, 0, obj.length);
    }

    public void setData(String[] data) {
        this.initialData = new String[data.length];
        System.arraycopy(data, 0, this.initialData, 0, data.length);
    }
}
