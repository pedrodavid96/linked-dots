package pt.isel.a41530.linkeddots.model;

public interface OnGameActionListener {

    void play();
    void onGameFinish();
    void pieceAction(int col, int lineOrigin, int lineDestination, PieceAction action);

    public enum PieceAction{
        NEW, SHIFT, CLEAR, UNDO;
    }
}
