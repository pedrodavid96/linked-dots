package pt.isel.a41530.linkeddots.model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;

import pt.isel.a41530.linkeddots.model.pieces.Point;

public class IOManager {

    public static LevelManager loadLevel(BufferedReader reader) throws IOException {
        LevelManager manager;

        String firstLine = reader.readLine();
        manager = createManager(firstLine);

        String secondLine = reader.readLine();
        if(secondLine.length()>1)
            manager.setObjectives(retrieveObjectives(secondLine));

        manager.setData(retrieveData(reader));

        return manager;
    }

    private static String[] retrieveData(BufferedReader reader) throws IOException{
        ArrayList<String> tempData = new ArrayList<>();
        String tempString;
        while ((tempString = reader.readLine()) != null){
            tempData.add(tempString.replace(" ",""));
        }
        String[] data = new String[tempData.size()];
        return tempData.toArray(data);
    }

    private static Objective[] retrieveObjectives(String secondLine) {
        String[] line;
        line = secondLine.split(" ");
        Objective[] objectives = new Objective[line.length];
        for (int i = 0; i < line.length; i++) {
            char c = line[i].charAt(0);
            String objective = "";
            String progress = "";
            for (int l = 1; l < line[i].length(); l++) {
                if (line[i].charAt(l) == '/') {
                    for (l++; l < line[i].length(); l++) {
                        progress += line[i].charAt(l);
                    }
                } else objective += line[i].charAt(l);
            }
            objectives[i] = new Objective(c, Integer.valueOf(objective));
            if (progress.length() > 0) {
                objectives[i].setProgress(Integer.valueOf(progress));
            }
        }
        return objectives;
    }

    private static LevelManager createManager(String firstLine) {
        String[] splicedLine = firstLine.split("[ x]");
        int cols = Integer.parseInt(splicedLine[0]);
        int lines = Integer.parseInt(splicedLine[1]);
        int playsRemaining = Integer.parseInt(splicedLine[2]);
        int score = 0;
        if(splicedLine.length > 3)
            score = Integer.parseInt(splicedLine[3]);
        return new LevelManager(cols, lines, playsRemaining, score);
    }

    public static void saveLevel(BufferedWriter writer, LinkedDots model) throws IOException{
        /* Write top Line (Lines"x"Cols" "PlaysRemaining" "Score) */
        writer.write(model.getCols() + "x" + model.getLines()
                + " " + model.getPlays() + " " + model.getScore());

        /* Write next Line (Objectives: char+Amount" ") */
        writer.newLine();
        Objective[] objectives = model.getObjectives();
        if(objectives!=null) {
            for (Objective obj : objectives) {
                writer.write(obj.toString() + " ");
            }
        }

        /* Write next "Line" (model) */
        writer.newLine();
        writer.write(model.toString());
    }

    public static ArrayList<Integer> saveComboData(LinkedDots model){
        LinkedList<Point> connectedPieces = model.getCombo();
        ArrayList<Integer> comboData = new ArrayList<>(connectedPieces.size()*2);
        for (Point piece : connectedPieces){
            comboData.add(piece.getX());
            comboData.add(piece.getY());
        }
        return comboData;
    }

    public static void loadComboData(LinkedDots model, ArrayList<Integer> data){
        int x, y;
        for(int i=0; i<data.size(); i+=2){
            x = data.get(i);
            y = data.get(i+1);
            model.addPieceToCombo(x, y);
        }
    }
}