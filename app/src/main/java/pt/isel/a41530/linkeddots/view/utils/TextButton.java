package pt.isel.a41530.linkeddots.view.utils;

import android.content.Context;
import android.widget.Button;

public class TextButton extends Button {
    public TextButton(Context context, String text){
        super(context);
        setText(text);
    }
}
