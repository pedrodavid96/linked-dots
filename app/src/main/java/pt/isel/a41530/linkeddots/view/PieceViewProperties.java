package pt.isel.a41530.linkeddots.view;

import android.graphics.Color;

import java.util.HashMap;

public class PieceViewProperties {

    private static HashMap<Character, PieceViewDetails> detailsMap = new HashMap<>();

    static {
        detailsMap.put('A', new PieceViewDetails(Shape.CIRCLE, Color.RED));
        detailsMap.put('B', new PieceViewDetails(Shape.CIRCLE, Color.BLUE));
        detailsMap.put('C', new PieceViewDetails(Shape.CIRCLE, Color.GREEN));
        detailsMap.put('D', new PieceViewDetails(Shape.CIRCLE, Color.GRAY));
        detailsMap.put('E', new PieceViewDetails(Shape.CIRCLE, Color.BLACK));
        detailsMap.put('F', new PieceViewDetails(Shape.CIRCLE, Color.CYAN));
        detailsMap.put('G', new PieceViewDetails(Shape.CIRCLE, Color.MAGENTA));
        detailsMap.put('?', new PieceViewDetails(Shape.CIRCLE, Color.WHITE, "?"));
        detailsMap.put('X', new PieceViewDetails(Shape.X_SQUARE, Color.GRAY));
        detailsMap.put('#', new PieceViewDetails(Shape.ROUND_SQUARE, Color.GRAY));
    }

    private static PieceViewDetails getPiece(char c) throws TypeNotFoundException{
        PieceViewDetails viewDetails = detailsMap.get(Character.valueOf(c));
        if(viewDetails != null){
            return viewDetails;
        }
        throw new TypeNotFoundException(c);
    }

    public static int getColor(char c){
        return getPiece(c).getColor();
    }

    public static Shape getShape(char c){
        return getPiece(c).getShape();
    }

    public static String getText(char c) {
        return getPiece(c).getText();
    }

    private static class PieceViewDetails{

        private final Integer COLOR;
        private final Shape SHAPE;
        private final String TEXT;

        public PieceViewDetails(Shape shape, Integer color, String text){
            this.COLOR = color;
            this.SHAPE = shape;
            this.TEXT = text;
        }

        public PieceViewDetails(Shape shape, Integer color){
            this(shape, color, "");
        }

        public Integer getColor(){
            return COLOR;
        }

        public Shape getShape(){
            return SHAPE;
        }

        public String getText(){
            return TEXT;
        }
    }
}