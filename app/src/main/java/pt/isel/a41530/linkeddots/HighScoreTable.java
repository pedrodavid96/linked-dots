package pt.isel.a41530.linkeddots;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;

public class HighScoreTable extends ArrayList<Statistic> {

    public HighScoreTable(){

    }

    public HighScoreTable(BufferedReader reader, ArrayList<Integer> levels) throws IOException {
        checkLevelsInFile(reader);
        checkNewLevels(levels);
    }

    private void checkLevelsInFile(BufferedReader reader) throws IOException {
        String tempString;
        String[] values;
        while((tempString = reader.readLine()) != null){
            values = tempString.split(" ");
            Statistic tempStatistic = new Statistic(Integer.valueOf(values[0]));
            int score = Integer.valueOf(values[1]);
            tempStatistic.setScore(score);
            if(score>0){
                tempStatistic.setUser(values[2]);
                tempStatistic.setDate(values[3]);
            }
            add(tempStatistic);
        }
    }

    private void checkNewLevels(ArrayList<Integer> levels) {
        outer: for(Integer level : levels){
            for(Statistic statistic: this){
                if(level == statistic.getLevel()){
                    continue outer;
                }
            }
            add(new Statistic(level));
        }
    }

    public void addStatistic(int level, int score, String user){
        for(Statistic statistic : this){
            if(statistic.getLevel()==level){
                statistic.setScore(score);
                statistic.setUser(user);
                statistic.setCurrentDate();
                return;
            }
        }
        Statistic tempStatistic = new Statistic(level);
        tempStatistic.setScore(score);
        tempStatistic.setUser(user);
        tempStatistic.setCurrentDate();
        add(tempStatistic);
    }

    public void save(BufferedWriter writer) throws IOException{
        for (Statistic tempStatistic: this){
            writer.write(tempStatistic.toString());
            writer.newLine();
        }
    }

    public int getMaxScore(int level){
        for(Statistic statistic : this){
            if(statistic.getLevel()==level){
                return statistic.getScore();
            }
        }
        return 0;
    }
}
