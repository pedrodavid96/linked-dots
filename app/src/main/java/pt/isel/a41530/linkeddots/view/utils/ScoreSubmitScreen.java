package pt.isel.a41530.linkeddots.view.utils;

import android.content.Context;
import android.graphics.Color;
import android.text.InputFilter;
import android.view.Gravity;
import android.widget.EditText;

import pt.isel.a41530.linkeddots.R;

public class ScoreSubmitScreen extends OrientedLayout{

    private static final int DEFAULT_TEXT_SIZE = 30, DEFAULT_MIN_TEXT_SIZE = 20,
            DEFAULT_MAX_ELMS = 10;
    private static final String DEFAULT_TEXT = "UNKNOWN";

    private EditText nickName;

    public ScoreSubmitScreen(Context context){
        super(context, VERTICAL);
        TextView newHighScore = new TextView(context, getResources().getString(R.string.newHighScore));
        newHighScore.setTextSize(DEFAULT_TEXT_SIZE);
        newHighScore.setTextColor(Color.argb(0xF0, 0x23, 0x91, 0x30));
        newHighScore.setGravity(Gravity.CENTER);

        OrientedLayout layout = new OrientedLayout(context, HORIZONTAL);

        TextView name = new TextView(context, getResources().getString(R.string.name));
        name.setTextSize(DEFAULT_MIN_TEXT_SIZE);

        nickName = new EditText(context);
        nickName.setText(DEFAULT_TEXT);
        nickName.setFilters(new InputFilter[]{new InputFilter.LengthFilter(DEFAULT_MAX_ELMS)});

        layout.addView(name);
        layout.addView(nickName);
        addView(newHighScore);
        addView(layout);
    }

    public String getNickName(){
        return nickName.getText().toString();
    }
}
