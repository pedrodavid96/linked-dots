package pt.isel.a41530.linkeddots.view.utils;

import android.content.Context;
import android.graphics.Typeface;

public class TextView extends android.widget.TextView{

    private static final int DEFAULT_TEXT_SIZE = 15;

    public TextView(Context context, String text) {
        super(context);
        setTextSize(DEFAULT_TEXT_SIZE);
        setTypeface(Typeface.create(Typeface.MONOSPACE, Typeface.NORMAL));
        setText(text);
    }
}
